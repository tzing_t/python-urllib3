%global srcname urllib3
%bcond_without tests

Name:           python-%{srcname}
Version:        1.26.18
Release:        1
Summary:        Sanity-friendly HTTP client for Python
License:        MIT
URL:            https://urllib3.readthedocs.io
Source0:        https://github.com/urllib3/urllib3/archive/refs/tags/%{version}.tar.gz
Source1:        ssl_match_hostname_py3.py

Patch0001:      remove_mock.patch

BuildArch:      noarch

%description 
HTTP library with thread-safe connection pooling, file post support, sanity friendly, and more. 

%package -n     python3-%{srcname}
Summary:        Python3 HTTP library with thread-safe connection pooling and file post

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%if %{with tests}
BuildRequires:  python3-cryptography python3-mock python3-six python-idna python-dateutil
BuildRequires:  python3-pysocks python3-pytest python3-tornado python-trustme
%endif

Requires:       ca-certificates python3-idna python3-six python3-pysocks

%description -n python3-urllib3
Python3 HTTP module with connection pooling and file POST abilities.

%prep
%autosetup -n %{srcname}-%{version} -p1

recent_date=$(date --date "7 month ago" +"%Y, %_m, %_d")
sed -i "s/^RECENT_DATE = datetime.date(.*)/RECENT_DATE = datetime.date($recent_date)/" src/urllib3/connection.py

#Failure use case circumvention
rm -rf test/with_dummyserver/
rm -rf test/appengine/
rm -rf test/contrib/
rm -rf test/test_no_ssl.py

%build
%py3_build

%install
%py3_install

rm -rf %{buildroot}%{python3_sitelib}/urllib3/packages/six.py
rm -rf %{buildroot}%{python3_sitelib}/urllib3/packages/__pycache__/six.*
rm -rf %{buildroot}%{python3_sitelib}/urllib3/packages/ssl_match_hostname/

mkdir -p %{buildroot}/%{python3_sitelib}/urllib3/packages/
cp -a %{SOURCE1} %{buildroot}/%{python3_sitelib}/urllib3/packages/ssl_match_hostname.py
ln -s %{python3_sitelib}/six.py %{buildroot}/%{python3_sitelib}/urllib3/packages/six.py
ln -s %{python3_sitelib}/__pycache__/six.cpython-%{python3_version_nodots}.opt-1.pyc %{buildroot}/%{python3_sitelib}/urllib3/packages/__pycache__/
ln -s %{python3_sitelib}/__pycache__/six.cpython-%{python3_version_nodots}.pyc %{buildroot}/%{python3_sitelib}/urllib3/packages/__pycache__/

%if %{with tests}
%check
# skip some failing tests
skiplist+="test_retry_deprecated or test_retry"
PYTHONPATH=%{buildroot}%{python3_sitelib}:%{python3_sitelib} %{__python3} -m pytest -v -k "not (${skiplist})"
%endif

%files -n python3-urllib3
%defattr(-,root,root)
%license LICENSE.txt
%doc CHANGES.rst README.rst
%{python3_sitelib}/urllib3/
%{python3_sitelib}/urllib3-*.egg-info

%changelog
* Tue Feb 06 2024 chengyechun <chengyechun1@huawei.com> - 1.26.18-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update python-urllib3 to version 1.26.18

* Tue Oct 31 2023 chengyechun <chengyechun1@huawei.com> - 1.26.17-2
- Type:CVE
- ID:CVE-2023-45803
- SUG:NA
- DESC:fix CVE-2023-45803 Made body stripped form HTTP requests

* Wed Oct 04 2023 Funda Wang <fundawang@yeah.net> - 1.26.17-1
- Update to version 1.26.17 to fix CVE-2023-43804

* Tue Aug 01 2023 chengyechun <chengyechun1@huawei.com> - 1.26.16-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:delete the unnecessary patch

* Mon Jul 31 2023 chengyechun <chengyechun1@huawei.com> - 1.26.16-1
- Type:enhancement
- CVE:NA
- SUG:NA
- DESC:update to 1.26.16

* Tue Jul 25 2023 chengyechun <chengyechun1@huawei.com> - 1.26.12-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix test_ssl_object_attributes

* Tue Mar 21 2023 chenhaixing <chenhaixing@huawei.com> - 1.26.12-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix socket timeout value when HTTPConnect is reused
       remove Exclamation mark character from the unreserved characters
       fix _idna_encode handling of x80

* Fri Feb 10 2023 chenhaixing <chenhaixing@huawei.com> - 1.26.12-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:change _HOST_PORT_PAT to fix port is 0 but return None

* Sat Dec 17 2022 chenhaixing <chenhaixing@huawei.com> - 1.26.12-2
- DESC:fix util.parse_url function traceback when port have leading zeros

* Sun Sep 04 2022 tianlijing <tianlijing@kylinos.cn> - 1.26.12-1
- update to 1.26.12

* Tue Aug 09 2022 chenhaixing <chenhaixing@huawei.com> - 1.26.8-2
- delete patch Bump-RECENT_DATE.patch and modify RECENT_DATE in python-urllib3.spec to adapt time change

* Mon Jul 18 2022 OpenStack_SIG <openstack@openeuler.org> - 1.26.8-1
- Upgrade package to version 1.26.8

* Wed Jul 06 2022 chenhaixing <chenhaixing@huawei.com> - 1.26.7-2
- add patch Bump RECENT_DATE and solve test case test_recent_date failed

* Wed Jan 19 2022 wangkerong <wangkerong@huawei.com> - 1.26.7-1
- update to 1.26.7 and remove python-nose dependency

* Mon Jul 5 2021 zhanzhimin <zhanzhimin@huawei.com> - 1.26.3-3
- fix CVE-2021-33503

* Tue Jun 1 2021 hanhui <hanhui15@huawei.com> - 1.26.3-2
- fix CVE-2021-28363

* Wed Feb 3 2021 chengguipeng <chengguipeng@huawei.com> - 1.26.3-1
- upgrade to 1.26.3

* Tue Jul 28 2020 chengguipeng<chengguipeng@huawei.com> - 1.25.9-1
- upgrade to 1.25.9

* Sat Oct 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.24.3-2
- optimize spec file.

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.24.3-1
- Package init
